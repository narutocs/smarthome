var mongoose = require("mongoose");

var schema = mongoose.Schema;

var homeSchema = new schema({
    _id : schema.Types.ObjectId,
    living : {
        statusLight : {
            type : Boolean,
            default : false
        },
        fan : {
            type : Boolean,
            default : false
        }
    },
    kitchen : {
        statusLight : {
            type : Boolean,
            default : false
        }
    },
    bath : {
        statusLight : {
            type : Boolean,
            default : false
        }
    }, 
    bed : {
        statusLightMain :{
            type : Boolean,
            default : false
        },
        statusLightExtra : {
            type : Boolean,
            default : false
        }
    },
    parameter : {
        temperature : {
            type : Number,
            default : 40
        },
        humidity : {
            type : Number,
            default : 40
        }
    },
    door : {
        status : {
            type : Boolean,
            default : false
        }
    },
    security : {
        status : {
            type : Boolean,
            default : false
        },
        values : {
            type : Boolean,
            default : false
        }
    }
})

var homeSchema = mongoose.model("HOME",homeSchema);

module.exports = homeSchema;