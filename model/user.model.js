var mongoose = require("mongoose");

var schema = mongoose.Schema;

var userSchema = new schema({
    _id : schema.Types.ObjectId,
    username : String,
    password : String,
    fullname : String
})

var userModel = mongoose.model("USER",userSchema);

module.exports = userModel;