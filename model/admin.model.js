var mongoose = require("mongoose");
var schema = mongoose.Schema;

var adminSchema = new schema({
    _id : schema.Types.ObjectId,
    username : String,
    fullname : String,
    email : String,
    password : String,
    phone : String,
    users : [{
        type : schema.Types.ObjectId,
        ref : "USER"
    }]
});

var adminModel = mongoose.model("ADMIN",adminSchema);

module.exports = adminModel;


