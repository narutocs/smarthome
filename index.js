var express = require("express");
var app = express();
var cron = require("cron").CronJob;
var mongoose = require("mongoose");
var userController = require("./controller/user.controller");
var homeController = require("./controller/home.controller");
var adminController = require("./controller/admin.controller");

app.use(express.static("public"));
app.set("view engine", "ejs");
app.set("views", "./views");
var server = require("http").createServer(app);
var io = require("socket.io")(server);

// mongoose.connect("mongodb://localhost:27017/smarthome");
mongoose.connect("mongodb+srv://admin:hahachuse@webdabong-k8fcp.mongodb.net/WebDaBong?retryWrites=true&w=majority");
userController.createAdmin();
server.listen(process.env.PORT || 3000, "192.168.2.74");
console.log("SERVER START IN PORT 3000");

homeController.saveInfo();
//homeController.getInfo();
var lsIos = [];
var lsWeb = [];
var lsEsp = [];
var index = 0;
io.on("connection", function (socket) {
  socket.on("disconnect", function () {
    for (var i = 0; i < lsIos.length; i++) {
      if (lsIos[i].socket == socket) {
        lsIos.splice(i, 1);
        console.log("1 thiết bị IOS đã ngắt kết nối hiện có " + lsIos.length + " thiết bị");
      }
    }
    for (var i = 0; i < lsWeb.length; i++) {
      if (lsWeb[i].socket == socket) {
        lsWeb.splice(i, 1);
        console.log("1 thiết bị WEB đã ngắt kết nối hiện có " + lsWeb.length + " thiết bị");
      }
    }
    for (var i = 0; i < lsEsp.length; i++) {
      if (lsEsp[i].socket == socket) {
        lsEsp.splice(i, 1);
        console.log("1 thiết bị ESP đã ngắt kết nối hiện có " + lsEsp.length + " thiết bị");
      }
    }
  })
  console.log("client ket noi");
  socket.emit("send_device");
  socket.on("connected", data => {
    if (data.name.toLowerCase() == "ios") {
      var isHaveIos = false;
      for (var i = 0; i < lsIos.length; i++) {
        if (lsIos[i].socket == socket) {
          isHaveIos = true;
        }
      }
      if (!isHaveIos) {
        lsIos.push({
          name: "IOS",
          socket: socket
        });
        console.log("Thiết bị IOS mới đã kết nối hiện có " + lsIos.length + " thiết bị");
      }
    } else if (data.name.toLowerCase() == "web") {
      var isHaveWeb = false;
      for (var i = 0; i < lsWeb.length; i++) {
        if (lsWeb[i].socket == socket) {
          isHaveWeb = true;
        }
      }
      if (!isHaveWeb) {
        lsWeb.push({
          name: "WEB",
          socket: socket
        });
        console.log("Thiết bị WEB mới đã kết nối hiện có " + lsWeb.length + " thiết bị");
      }
    } else if (data.name.toLowerCase() == "esp8266") {
      var isHaveEsp = false;
      for (var i = 0; i < lsEsp.length; i++) {
        if (lsEsp[i].socket == socket) {
          isHaveEsp = true;
        }
      }
      if (!isHaveEsp) {
        lsEsp.push({
          name: "ESP",
          socket: socket
        });
        console.log("Thiết bị ESP mới đã kết nối hiện có " + lsEsp.length + " thiết bị");
      }
    }
    socket.on("create-new-user", user => {
      userController.createUserMenber(user.username, user.password, user.fullname)
        .then(data => {
          adminController.addUser(data._id)
            .then(() => {
              console.log("thành công");
              userController.getInfoAdmin("admin")
                .then(admin => {
                  socket.emit("thanh-cong",
                    {
                      "user": admin.users
                    });
                })
                .catch(err => {
                  console.log(err);
                })
            })
            .catch(err => {
              console.log(err);
              socket.emit("that-bai");
            })
        })
        .catch(err => {
          console.log(err);
          socket.emit("that-bai");
        })
    })
    // var isHave = false;
    // for(var i=0;i<arr.length;i++){
    //   if(arr[i].socket == socket){
    //     isHave = true;
    //   }
    // }
    // if(!isHave){
    //   arr.push({
    //     name : data.name,
    //     socket : socket
    //   })
    //   console.log(arr);
    // }
  });

  socket.emit("send_info");
  socket.on("receive_info", function (data) {

  })

  socket.on("living-light", function (data) {
    homeController.updateInfoLivingLight(data)
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        var status = "";
        if (data.status) {
          status = 'bật';
        } else {
          status = 'tắt';
        }

        console.log("Đèn phòng khách: " + status);
      })
      .catch(err => {
        console.log(err);
      })
  })
  socket.on("living-fan", function (data) {
    homeController.updateInfoLivingFan(data)
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        var status = "";
        if (data.status) {
          status = 'bật';
        } else {
          status = 'tắt';
        }

        console.log("Quạt phòng khách: " + status);
      })
      .catch(err => {
        console.log(err);
      })
  })


  socket.on("kitchen-light", function (data) {
    homeController.updateInfoKitchenLight(data)
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        var status = "";
        if (data.status) {
          status = 'bật';
        } else {
          status = 'tắt';
        }

        console.log("Đèn phòng bếp: " + status);
      })
      .catch(err => {
        console.log(err);
      })
  })


  socket.on("bath-light", function (data) {
    homeController.updateInfoBathLight(data)
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        var status = "";
        if (data.status) {
          status = 'bật';
        } else {
          status = 'tắt';
        }

        console.log("Đèn phòng tắm: " + status);
      })
      .catch(err => {
        console.log(err);
      })
  })

  socket.on("bed-light-main", function (data) {
    homeController.updateInfoBedLightMain(data)
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        var status = "";
        if (data.status) {
          status = 'bật';
        } else {
          status = 'tắt';
        }

        console.log("Đèn phòng ngủ: " + status);
      })
      .catch(err => {
        console.log(err);
      })
  })

  socket.on("bed-light-extra", function (data) {
    homeController.updateInfoBedLightExtra(data)
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        var status = "";
        if (data.status) {
          status = 'bật';
        } else {
          status = 'tắt';
        }

        console.log("Đèn ngủ: " + status);
      })
      .catch(err => {
        console.log(err);
      })
  })
  socket.on("door", function (data) {
    homeController.updateInfoDoor(data)
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        var status = "";
        if (data.status) {
          status = 'Mở';
        } else {
          status = 'Khóa';
        }

        console.log("Cửa: " + status);
      })
      .catch(err => {
        console.log(err);
      })
  })
  socket.on("deleteUser",data=>{
    userController.deleteUser(data.username)
    .then(result=>{
      adminController.deleteUser(result)
      .then(()=>{
        userController.getInfoAdmin("admin")
        .then(admin=>{
          for(var i=0;i<lsIos.length;i++){

            lsIos[i].socket.emit("deleteDone",{"admin" : admin});
          }
          for(var i=0;i<lsWeb.length;i++){
            lsWeb[i].socket.emit("deleteDone",{"admin" : admin});
          }
        })
        .catch(err=>{
          console.log(err);
        })
        
      })
      .catch(err=>{
        console.log(err);
      })
      console.log("Xóa thành công user " + data.username);
    })
    .catch(err=>{
      console.log(err);
    })
  })
  socket.on("login", function (data) {
    userController.login(data.username, data.password)
      .then((result) => {
        console.log("Đăng nhập thành công");
        userController.getInfoAdmin(data.username)
          .then(admin => {
            homeController.getInfo()
              .then(data => {
                socket.emit("login-thanh-cong",
                  {
                    "admin": admin,
                    "user": result.user,
                    "home": data
                  });
              })
              .catch(err => {
                console.log(err);
              })
          })
          .catch(err => {
            console.log(err);
          })

      })
      .catch(err => {
        console.log(err);
        socket.emit("login-that-bai");
      })

  })
  socket.on("forgot-password", data => {
    var newPass = Math.floor(Math.random() * (999999 - 100000 + 1) - 100000);
    userController.forgotPassword(data.email, newPass)
      .then(() => {
        console.log("reset thanh cong");
        socket.emit("reset-thanh-cong");
      })
      .catch(err => {
        console.log(err);
        socket.emit("reset-that-bai");
      })
  })
  socket.on("change-name", data => {
    userController.changeName(data.change)
      .then(data => {
        console.log("Thay đổi thành công")
      })
      .catch(err => {
        console.log(err);
      })
  })
  socket.on("change-phone", data => {
    userController.changePhone(data.change)
      .then(data => {
        console.log("Thay đổi thành công")
      })
      .catch(err => {
        console.log(err);
      })
  })
  socket.on("off-all-light",()=>{
    homeController.updateInfoLivingLight({status:false})
    .then(data => {
      for (var i = 0; i < lsEsp.length; i++) {
        lsEsp[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsWeb.length; i++) {
        lsWeb[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsIos.length; i++) {
        lsIos[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
    })
      
      console.log("Đèn phòng khách: " + "Bật");
      homeController.updateInfoKitchenLight({status:false})
    .then(data => {
      for (var i = 0; i < lsEsp.length; i++) {
        lsEsp[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsWeb.length; i++) {
        lsWeb[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsIos.length; i++) {
        lsIos[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
    })
      console.log("Đèn phòng bếp: " + "Bật");
      homeController.updateInfoBathLight({status:false})
    .then(data => {
      for (var i = 0; i < lsEsp.length; i++) {
        lsEsp[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsWeb.length; i++) {
        lsWeb[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsIos.length; i++) {
        lsIos[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
    })
      console.log("Đèn phòng tắm: " + "Bật");
      homeController.updateInfoBedLightMain({status:false})
    .then(data => {
      for (var i = 0; i < lsEsp.length; i++) {
        lsEsp[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsWeb.length; i++) {
        lsWeb[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsIos.length; i++) {
        lsIos[i].socket.emit("light", {
          den: data.den,
          status: data.status + ""
        });
      }

      console.log("Đèn phòng ngủ: " + "Bật");
    })
  })
  socket.on("logout", () => {
    for (var i = 0; i < lsIos.length; i++) {
      if (lsIos[i].socket == socket) {
        lsIos.splice(i, 1);
        console.log("1 thiết bị IOS đã ngắt kết nối hiện có " + lsIos.length + " thiết bị");
      }
    }
    for (var i = 0; i < lsWeb.length; i++) {
      if (lsWeb[i].socket == socket) {
        lsWeb.splice(i, 1);
        console.log("1 thiết bị WEB đã ngắt kết nối hiện có " + lsWeb.length + " thiết bị");
      }
    }
    for (var i = 0; i < lsEsp.length; i++) {
      if (lsEsp[i].socket == socket) {
        lsEsp.splice(i, 1);
        console.log("1 thiết bị ESP đã ngắt kết nối hiện có " + lsEsp.length + " thiết bị");
      }
    }
  })
  index++;
  if (index == 1) {
    // var jobGetData = new cron({
    //   //cronTime :  '*/15 * * * * *', // chạy vào 00: 00 hang dem
    //   cronTime: '*/30 * * * * *', // test
    //   onTick: function () {
    //     homeController.getInfo()
    //       .then(data => {
    //         for (var i = 0; i < lsWeb.length; i++) {
    //           lsWeb[i].socket.emit("home-update",{"home": data});
    //         }
    //         for (var i = 0; i < lsIos.length; i++) {
    //           lsIos[i].socket.emit("home-update", {"home": data});
    //         }
            
    //       })
    //       .catch(err => {
    //         console.log(err);
    //       })

    //     console.log('đã gửi data cho web và ios');
    //   },
    //   start: true,
    //   timeZone: 'Asia/Ho_Chi_Minh'
    // });

   

    // var jobUpdateData = new cron({
    //   cronTime :  '*/30 * * * * *', // chạy vào 00: 00 hang dem
    //  // cronTime :  '*/45 * * * * *', // test
    //   onTick : function(){

    //         lsEsp[i].socket.on("data_weather",data=>{
    //           homeController.updateParameter(data)
    //           .then(()=>{ 
    //             console.log('đã update thông tin weather ' + data.temp + " - " + data.hump);
    //           })
    //           .catch(err=>{
    //             console.log(err);
    //           })
    //         })


    //   },
    //   start : true,
    //   timeZone : 'Asia/Ho_Chi_Minh'
    // });


//    jobGetData.start();

    // jobUpdateData.start();
  }

socket.on("data_weather",data=>{
    homeController.updateParameter(data)
      .then(() => {
        homeController.getInfo()
        .then(data => {
          for (var i = 0; i < lsWeb.length; i++) {
            lsWeb[i].socket.emit("home-update",{"home": data});
          }
          for (var i = 0; i < lsIos.length; i++) {
            lsIos[i].socket.emit("home-update", {"home": data});
          }
          
        })
        .catch(err => {
          console.log(err);
        })
      })
      .catch(err => {
        console.log(err);
      })
  })
  

  socket.on("alarm", data => {
    console.log(data.alarm);
    homeController.updateInfoSecurityAuto(data)
    .then(result=>{
      if(data.alarm == 1){
        homeController.getInfo()
        .then(dataHome=>{
          if(dataHome.security.status == true){
            for (var i = 0; i < lsWeb.length; i++) {
              lsWeb[i].socket.emit("checkSecurity", {
                status: data.alarm
              });
            }
            for (var i = 0; i < lsIos.length; i++) {
              lsIos[i].socket.emit("checkSecurity", {
                status: data.alarm
              });
            }
          }
        })
        .catch(err=>{
          console.log(err);
        })
        homeController.updateInfoLivingLight({status:true})
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        
        
        console.log("Đèn phòng khách: " + "Bật");
        homeController.updateInfoKitchenLight({status:true})
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }

        console.log("Đèn phòng bếp: " + "Bật");
        homeController.updateInfoBathLight({status:true})
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }

        console.log("Đèn phòng tắm: " + "Bật");
        homeController.updateInfoBedLightMain({status:true})
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }

        console.log("Đèn phòng ngủ: " + "Bật");
        homeController.updateInfoDoor({status:false})
      .then(data => {
        for (var i = 0; i < lsEsp.length; i++) {
          lsEsp[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsWeb.length; i++) {
          lsWeb[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        for (var i = 0; i < lsIos.length; i++) {
          lsIos[i].socket.emit("light", {
            den: data.den,
            status: data.status + ""
          });
        }
        console.log("Cửa: " + "Đóng");
      })
      .catch(err => {
        console.log(err);
      })
      })
      .catch(err => {
        console.log(err);
      })
      })
      .catch(err => {
        console.log(err);
      })
      })
      .catch(err => {
        console.log(err);
      })
      })
      .catch(err => {
        console.log(err);
      })
      }
      console.log("update thông tin security thành công");
    })
    .catch(err=>{
      console.log(err);
    })
  })
  // setTimeout(() => {
  //   () => {
  //     for(var i=0;i<lsEsp.length;i++){
  //       lsEsp[i].socket.on("data_weather", data => {
  //         homeController.updateParameter(data)
  //           .then(() => {
  //             console.log('đã update thông tin weather ' + data.temp + " - " + data.hump);
  //           })
  //           .catch(err => {
  //             console.log(err);
  //           })
  //       })
  //     }
      
  //   }
  // }, 30000)
  socket.on("security",data=>{
    homeController.updateInfoSecurity(data)
    .then(data=>{
      for (var i = 0; i < lsWeb.length; i++) {
        lsWeb[i].socket.emit("light", {
          den: 8,
          status: data.status + ""
        });
      }
      for (var i = 0; i < lsIos.length; i++) {
        lsIos[i].socket.emit("light", {
          den: 8,
          status: data.status + ""
        });
      }
    })
    .catch(err=>{
      console.log(err);
    })
  })
})