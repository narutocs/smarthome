var userModel = require("../model/user.model");
var adminModel = require("../model/admin.model");

var cons = require("../constant/cons");
var errorMessage = require("../constant/erro");
var crypto = require("crypto");
var mongoose = require("mongoose");
var nodemailer = require("nodemailer");


function addUser(user){
    return adminModel.find()
    .then(admin=>{
        if(admin.length >0 ){
            var lsUser = admin[0].users;
            lsUser.push(user);
            return adminModel.update({_id:admin[0]._id},{users:lsUser})
            .then(data=>{
                return Promise.resolve(lsUser);
            })
            .catch(err=>{
                return Promise.reject(err);
            })
        }
    })
    .catch(err=>{
        return Promise.reject(eer);
    })
}
function deleteUser(user){
    return adminModel.find()
    .then(admin=>{
        if(admin.length>0){
            var lsUser = admin[0].users;
            for(var i=0;i<lsUser.length;i++){
                if(lsUser[i] == user){
                    lsUser.splice(i,1);
                }
            }
            return adminModel.update({_id:admin[0]._id},{users:lsUser})
            .then(data=>{
                return Promise.resolve(lsUser);
            })
            .catch(err=>{
                return Promise.reject(err);
            })
        }
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
module.exports = {
    addUser : addUser,
    deleteUser : deleteUser
}