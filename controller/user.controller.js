var userModel = require("../model/user.model");
var adminModel = require("../model/admin.model");

var cons = require("../constant/cons");
var errorMessage = require("../constant/erro");
var crypto = require("crypto");
var mongoose = require("mongoose");
var nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    auth: {
        user: "webdabong@gmail.com",
        pass: "hahachuse123"
    }
})

function forgotPassword(email, password) {
    return transporter.sendMail({
        from: "Smart Home",
        to: email,
        subject: "Cấp lại mật khẩu mới",
        html: "<h1>" + "mật khẩu mới của bạn là " + password + "</h1>"
    })
        .then(() => {
            return adminModel.find({ email })
                .then(function (user) {
                    if (user.length > 0) {
                        var hash = crypto.createHmac('sha256', cons.keyPassword)
                            .update(password + "")
                            .digest('hex');
                        return userModel.update({ _id: user[0]._id }, { password: hash })
                            .then(() => {
                                return Promise.resolve({
                                    message: errorMessage.resetPasswordThanhCong
                                });
                            })
                            .catch(function (err) {
                                return Promise.reject(err);
                            })
                    }
                    else {
                        return Promise.reject({
                            statusCode: 400,
                            message: errorMessage.emailNotFound
                        })
                    }
                })
                .catch(function (err) {
                    return Promise.reject(err);
                })
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function login(username, password) {
    return adminModel.find({ username })
        .then(function (user) {
            if (user.length > 0) {
                var hash = crypto.createHmac('sha256', cons.keyPassword)
                    .update(password)
                    .digest('hex');
                if (hash == user[0].password) {
                    return Promise.resolve({
                        user : user[0]
                    })
                }
                else {
                    return Promise.reject({
                        statusCode: 400,
                        message: errorMessage.dangNhapFail
                    })
                }
            } else {
                return userModel.find({ username })
                    .then(function (user) {
                        if (user.length > 0) {
                            var hash = crypto.createHmac('sha256', cons.keyPassword)
                                .update(password)
                                .digest('hex');
                            if (hash == user[0].password) {
                                return Promise.resolve({
                                    user : user[0]
                                })
                            }
                            else {
                                return Promise.reject({
                                    statusCode: 400,
                                    message: errorMessage.dangNhapFail
                                })
                            }
                        }
                    })
                    .catch(function (err) {
                        return Promise.reject(err);
                    })
            }
        })
        .catch(err => {
            return Promise.reject(err);
        })
}
function createUser(username, password, email, fullname, phone, users) {
    return adminModel.find({ email: email })
        .then(function (user) {
            if (user.length > 0) {
                return Promise.reject({
                    statusCode: 400,
                    message: errorMessage.userTonTai
                })
            } else {
                return adminModel.find({ username: username })
                    .then(function (data) {
                        if (data.length > 0) {
                            return Promise.reject({
                                statusCode: 400,
                                message: errorMessage.userTonTai
                            })
                        } else {
                            var hash = crypto.createHmac('sha256', cons.keyPassword)
                                .update(password)
                                .digest('hex');
                            password = hash;
                            var admin = new adminModel({
                                _id: mongoose.Types.ObjectId(),
                                username: username,
                                password: password,
                                email: email,
                                phone: phone,
                                fullname: fullname,
                                users: users
                            });
                            return admin.save()
                                .then(function (admin) {
                                    return Promise.resolve(admin);
                                })
                                .catch(function (err) {
                                    return Promise.reject(err);
                                })
                        }
                    })
                    .catch(function (err) {
                        return Promise.reject(err);
                    })
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function createAdmin() {
    adminModel.find()
        .then(function (user) {
            if (user.length == 0) {
                createUser("admin", "admin", "nhutcs123@gmail.com", "Hà Hữu Nhựt", "0386741177", [])
                    .then(function (data) {
                        console.log(data);
                    })
                    .catch(function (err) {
                        console.log(err);
                    })
            }
        })
        .catch(function (err) {
            console.log(err)
        })
}
function changeName(name) {
    return adminModel.find()
        .then(user => {
            if (user.length > 0) {
                return adminModel.update({ _id: user[0]._id }, { fullname: name })
                    .then(data => {
                        return Promise.resolve(data);
                    })
                    .catch(err => {
                        return Promise.reject(err);
                    })
            } 
        })
        .catch(err => {
            return Promise.reject(err);
        })
}

function changePhone(phone) {
    return adminModel.find()
        .then(user => {
            if (user.length > 0) {
                return adminModel.update({ _id: user[0]._id }, { phone: phone })
                    .then(data => {
                        return Promise.resolve(data);
                    })
                    .catch(err => {
                        return Promise.reject(err);
                    })
            }
        })
        .catch(err => {
            return Promise.reject(err);
        })
}

function getInfoAdmin(username) {
    return adminModel.find({ username }).populate("users")
        .then(admin => {
            return Promise.resolve(admin[0]);
        })
        .catch(err => {
            return Promise.reject(err);
        })
}

function deleteUser(username){
    return userModel.find({username})
    .then(user=>{
        return userModel.deleteOne({username})
        .then(()=>{
            return Promise.resolve(user[0].id);
        })
        .catch(err=>{
            return Promise.reject(err);
        })
        
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}

function createUserMenber(username, password, fullname) {
    return userModel.find({ username: username })
        .then(function (user) {
            if (user.length > 0) {
                return Promise.reject({
                    statusCode: 400,
                    message: errorMessage.userTonTai
                })
            } else {
                return userModel.find({ username: username })
                    .then(function (data) {
                        if (data.length > 0) {
                            return Promise.reject({
                                statusCode: 400,
                                message: errorMessage.userTonTai
                            })
                        } else {
                            var hash = crypto.createHmac('sha256', cons.keyPassword)
                                .update(password)
                                .digest('hex');
                            password = hash;
                            var user = new userModel({
                                _id: mongoose.Types.ObjectId(),
                                username: username,
                                password: password,
                                fullname: fullname
                            });
                            return user.save()
                                .then(function (user) {
                                    return Promise.resolve(user);
                                })
                                .catch(function (err) {
                                    return Promise.reject(err);
                                })
                        }
                    })
                    .catch(function (err) {
                        return Promise.reject(err);
                    })
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}
module.exports = {
    createAdmin: createAdmin,
    createUser: createUser,
    login: login,
    forgotPassword: forgotPassword,
    getInfoAdmin: getInfoAdmin,
    changeName: changeName,
    changePhone: changePhone,
    createUserMenber: createUserMenber,
    deleteUser : deleteUser
}