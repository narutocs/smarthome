var homeModel = require("../model/home.model");
var mongoose = require("mongoose");
var cron = require("cron");
function getInfo(){
    return homeModel.find()
    .then(data=>{
        return Promise.resolve(data[0]);
    })
    .catch(err=>{   
        return Promise.reject(err);
    })
}
function saveInfo(data){
    homeModel.find()
    .then(data=>{
        if(data.length == 0){
            var home = new homeModel({
                _id: mongoose.Types.ObjectId()
            });
            home.save();
        }
    })
    
}
function updateInfoLivingLight(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{living : {statusLight: data.status,fan:home.living.fan}})
        .then(result=>{
            return Promise.resolve({
                den: 1,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
function updateInfoLivingFan(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{living : {statusLight: home.living.statusLight,fan:data.status}})
        .then(result=>{
            return Promise.resolve({
                den: 2,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
function updateInfoKitchenLight(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{kitchen : {statusLight: data.status}})
        .then(result=>{
            return Promise.resolve({
                den: 3,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
function updateInfoBathLight(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{bath : {statusLight: data.status}})
        .then(result=>{
            return Promise.resolve({
                den: 6,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
function updateInfoBedLightMain(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{bed : {statusLightMain: data.status,statusLightExtra:home.bed.statusLightExtra}})
        .then(result=>{
            return Promise.resolve({
                den: 4,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}

function updateInfoBedLightExtra(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{bed : {statusLightExtra: data.status,statusLightMain:home.bed.statusLightMain}})
        .then(result=>{
            return Promise.resolve({
                den: 5,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
function updateInfoDoor(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{door : {status : data.status}})
        .then(result=>{
            return Promise.resolve({
                den: 7,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
function updateParameter(data){
    return homeModel.update({},{parameter : {temperature : data.temp,humidity: data.hump}})
    .then(result=>{
        return Promise.resolve(result);
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
function updateInfoSecurity(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{security : {status : data.status,values : home.security.values}})
        .then(result=>{
            return Promise.resolve({
                den: 8,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    })
}
function updateInfoSecurityAuto(data){
    return getInfo()
    .then(home=>{
        return homeModel.update({},{security : {values : data.alarm, status : home.security.status}})
        .then(result=>{
            return Promise.resolve({
                den: 8,
                status : data.status
            });
        })
        .catch(err=>{
            return Promise.reject(err);
        })
    })
    .catch(err=>{
        return Promise.reject(err);
    }) 
}
module.exports = {
    getInfo : getInfo,
    saveInfo : saveInfo,
    updateInfoLivingLight: updateInfoLivingLight,
    updateInfoLivingFan : updateInfoLivingFan,
    updateInfoBathLight : updateInfoBathLight,
    updateInfoBedLightExtra : updateInfoBedLightExtra,
    updateInfoBedLightMain : updateInfoBedLightMain,
    updateInfoKitchenLight : updateInfoKitchenLight,
    updateInfoDoor : updateInfoDoor,
    updateParameter : updateParameter,
    updateInfoSecurity : updateInfoSecurity,
    updateInfoSecurityAuto : updateInfoSecurityAuto
}